# Aufgaben

## -1. Merge-Konflikte

Hallöchen

## 0. In GitLab einloggen, SSH-Key hinterlegen

Loggen Sie sich mit Ihrer Benutzerkennung in GitLab ein. Generieren Sie dann
über "ssh-keygen" einen SSH-Key und hinterlegen den öffentlichen Teil davon
in Ihrem GitLab-Profil.

## 1. Projekt forken

Forken Sie dieses Projekt direkt über die GitLab-Oberfläche, um eine eigenständige
Kopie in Ihrem privaten Namensraum zu bekommen.

## 2. Eigenen Fork klonen

Nun klonen Sie den soeben erstellten Fork auf Ihren Kursrechner. Benutzen Sie
dazu wahlweise die ssh- oder https-URL.

## 3. Dateien anlegen

Legen Sie im geklonten Projekt einen neuen Ordner an. Der Name ist dabei egal,
sollte aber individuell sein.

In diesem Ordner erstellen Sie nun eine Datei mit etwas Text.

Sobald Sie mit dem Text fertig sind, committen Sie ihn und pushen die Änderungen
am git-Repository ins GitLab.

## 4. Kollaborieren über Merge Requests

Wir wollen unsere Dateien nun sammeln. Dazu verwenden wir **Merge Requests**, um
unseren Fork wieder mit dem Hauptprojekt zu synchronisieren.

Erstellen Sie einen Merge Request von Ihrem Fork hin zum Hauptprojekt.

## 5. Kollaborieren über Branches

Kollaboration über Forks/Merge Requests bietet sich dann an, wenn es eine große
Anzahl von Personen gibt, die an einem Projekt arbeiten. In so einem Fall möchte
man nicht jeder einzelnen Person Schreibzugriff auf das zentrale Git-Repository
geben, alleine schon, um Chaos zu vermeiden.

Bei kleineren Projekten ist das etwas lockerer. Hier bietet es sich an, wenn alle
Personen am gleichen GitLab-Projekt mitarbeiten (ggf. mit unterschiedlichen
Berechtigungen). Damit man sich hier nicht in die Quere kommt, sollte jeder
seinen Code in **Branches** entwickeln.

### 5.1. Dateien anlegen

Klonen Sie das zentrale Repository, legen Sie einen neuen Branch mit passendem
Branchnamen an und führen Sie dann Schritt 2) erneut aus.

### 5.2. Merge Request erstellen

Erstellen Sie nun einen neuen Merge Request im Hauptprojekt. Wählen Sie dabei als
Quelle Ihren eigenen Branch und als Ziel den Master-Branch aus.

## 6. Release Management mit Tags

Um Software weiterhin mit Versionsnummern veröffentlichen zu können, kann
das "Tag"-Feature in git genutzt werden. Tags sind frei wählbare Namen für
individuelle Git-Commits. GitLab bietet derartige Tags direkt als .zip-Download
an.

Forken Sie das Projekt erneut (ggf. den alten Fork vorher löschen) und legen Sie
einmal über die GitLab-Oberfläche und dann über die Kommandozeile einen Tag an.

Pushen Sie den lokal angelegten Tag ins GitLab und machen Sie sich dort mit der
Tag-Übersicht vertraut.

## 7. Issues

GitLab bietet mit den Issues ein sehr nützliches Werkzeug an, um Projekte zu
verwalten. Damit lassen sich nicht nur Software-Bugs tracken, sondern auch
das Projekt selbst planen.

Machen Sie sich mit GitLab-Issues vertraut. Beachten Sie dabei das (an KanBan
angelehnte) Issue-Board und die Milestones.

## 8. Wiki

Jede GitLab-Projekt kommt mit einem Wiki. Das Wiki ist selbst auch nur ein
git-Projekt und kann über die Markdown-Syntax mit Inhalt befüllt werden.

Legen Sie eine Seite im Wiki an.
